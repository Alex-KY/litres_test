'use strict';
var loc = window.location.href;
var DOCUMENT_XML = loc.slice(loc.indexOf("?XML=/") + 6, loc.lastIndexOf(".xml") + 4);
var XHR = new XMLHttpRequest();

XHR.open("GET", DOCUMENT_XML, false);
XHR.setRequestHeader("Content-Type", "text/xml");
XHR.send(null);

var XMLDoc = XHR.responseXML;
var BODYS = XMLDoc.getElementsByTagName("body");
var Refs = XMLDoc.getElementsByTagName("a");

var re = /([А-Яа-яёA-Za-z])+/g;
var sumLettersInAllTags = 0;
var sumSymbolsInAllTags = 0;
for (var body of BODYS) {
	sumSymbolsInAllTags += body.textContent.replace(' ', '').length;
	var arrLetters = body.textContent.match(re);
	if (arrLetters) {
		for (var letters of arrLetters) {
			sumLettersInAllTags += letters.length;
		}
	}
}

var brokenRef = 0;
for (var link of Refs) {
	if (!XMLDoc.querySelector(link.getAttribute("l:href"))) {
		brokenRef++;
	}
}

document.querySelector("#localLinkCount").textContent = Refs.length;
document.querySelector("#sumLetters").textContent = sumLettersInAllTags;
document.querySelector("#sumSymbols").textContent = sumSymbolsInAllTags;
document.querySelector("#linkBrok").textContent = brokenRef;